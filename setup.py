from setuptools import find_packages, setup

with open("Readme.md", "r") as f:
  long_description = f.read()

setup(
  name="alphabet_soup",
  version="0.0.2",
  description="Provided with a grid and the list of words that have been hidden within it, the application will search for words, returning start and end coordinates",
  long_description=long_description,
  long_description_content_type="text/markdown",
  packages=find_packages(),
  url="https://gitlab.com/enlighten-challenge/alphabet-soup",
  author="Jonathan Sanchez",
  author_email="jsandroid@gmail.com",
  license="MIT",
  classifiers=[
    "License :: OSI Approved :: MIT License",
    "Programming Language :: Python :: 3.12",
    "Operating System :: OS Independent",
  ],
  install_requires=[],
  python_requires=">=3.12",
  entry_points={  
      "console_scripts": [
          "alphabet_soup=run:main",
      ],
  },
)