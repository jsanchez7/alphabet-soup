import os.path, sys
from alphabet_soup.alphabet_soup import read_and_solve

def main():
  if len(sys.argv) == 1:
    if os.path.isfile("input.txt"):
      filename = "input.txt" 
    else: 
      inputFile = input("Please enter filename to solve:").strip()
      while(not os.path.isfile(inputFile)):
        inputFile = input("File not found, please enter valid filename to solve:").strip()
      filename = inputFile
  else:
    filename = sys.argv[1]

  print(read_and_solve(filename).strip())
  

if __name__ == "__main__":
  main()