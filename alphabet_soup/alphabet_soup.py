class Trie:
  def __init__(self):
    self.isWordEnd = False
    self.map = {}
# function to add word to dictionary
def add_keyword(dictionary, word):

  for char in range(len(word)):
    letter = word[char]

    if letter not in dictionary.map:
      dictionary.map[letter] = Trie()

    dictionary = dictionary.map[letter]

  dictionary.isWordEnd = True

# function to search words in trie dictionary
# returns None if not found, "Exists" if substring found, or word if its a dictionary key
def search_keyword(dictionary, word):
  
  for char in range(len(word)):
    letter = word[char]
    if letter not in dictionary.map:
      return None
    dictionary = dictionary.map[letter]

  return word if dictionary.isWordEnd else "Exists"


# function to remove words from trie dictionary
def remove_keyword(dictionary, word):
  if len(word) == 1:
    if word in dictionary.map and dictionary.map[word].isWordEnd:
      dictionary.map.pop(word)
      return True
    return dictionary.isWordEnd
  else:
    if word[0] in dictionary.map and remove_keyword(dictionary.map[word[0]], word[1:]): 
      if not dictionary.map[word[0]].isWordEnd:
        dictionary.map.pop(word[0])
        return len(dictionary.map) < 1
      return False
    return False



# given a specified direction, will search for any instance of a key word
def search_in_direction(grid, r, c, direction, dictionary, word_reversed = False):
  tmp_term = ''
  row, col = len(grid), len(grid[0])
  row_index, col_index = r, c
  while 0 <= row_index < row and 0 <= col_index < col:
    tmp_term += grid[row_index][col_index]
    search_result = search_keyword(dictionary, tmp_term)
    match search_result:
      case None:
        return "",""
      case "Exists":
        row_index += direction[0]
        col_index += direction[1]
      case _:
        if word_reversed:
          search_result = search_result[::-1]
          rev_r, rev_c = r, c
          r, c = row_index, col_index
          row_index, col_index = rev_r, rev_c
        return f'{r}:{c} {row_index}:{col_index}', f'{search_result}'
  return "",""

# logic to solve the puzzle, will search for key words from each cell until all keys are found or all cells were covered
def solve_puzzle(grid, keys, solution):
  # possible directions to search through the grid
  direction =[[0,1], [1,1], [1, 0],[1, -1], [0, -1], [-1, -1], [-1,1], [-1, 0]]
  for r in range (0, len(grid)):
    for c in range (0, len(grid[0])):
      for dir in range(0, len(direction)):
        if(len(keys[0].map) == 0 and len(keys[1].map) == 0): return solution
        result, key = search_in_direction(grid, r, c, direction[dir], keys[0])
        if len(key) == 0:
          result, key = search_in_direction(grid, r, c, direction[dir], keys[1], True)
        if len(key) > 0:
          solution.update({key: result})
          remove_keyword(keys[0], key)
          remove_keyword(keys[1], key[::-1])
  return solution

# takes a file name to read and solves it, assumes proper formatting of file
def read_and_solve(filename):
  # read file and remove carriage return
  with open(filename, "r") as f:
    data = [line.rstrip() for line in f]

  # store row and column
  row, _ = (int(i) for i in data[0].split('x'))

  # store word puzzle
  grid =[[] for _ in range(row)]
  for index in range (1, row + 1):
    grid[index - 1] = data[index].split()

  # build trie dictionary
  forward = Trie()
  backward = Trie()

  # store words keys
  keys = {}
  for i in range(row + 1, len(data)):
    word = data[i]
    b_word = data[i][::-1]
    keys[word] = "Not Found"
    add_keyword(forward, word)
    add_keyword(backward, b_word)

  # solve puzzle and print results
  results = solve_puzzle(grid, [forward, backward], keys)
  return ''.join([f'{key} {value}\n' for key, value in results.items()])

