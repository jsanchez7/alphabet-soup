import unittest
from alphabet_soup.alphabet_soup import (
  Trie, 
  add_keyword, 
  search_keyword, 
  remove_keyword, 
  read_and_solve,
)

class Testalphabet_soup(unittest.TestCase):

  def setUp(self):
    self.dictionary = Trie()
  
  def test_add_keyword(self):
    print('test_add_keyword\n')
    add_keyword(self.dictionary, "abc")
    self.assertTrue(self.dictionary.map['a'].map['b'].map['c'].isWordEnd)
    add_keyword(self.dictionary, "ab")
    self.assertTrue(self.dictionary.map['a'].map['b'].isWordEnd)
    add_keyword(self.dictionary, "ard")
    self.assertTrue(self.dictionary.map['a'].map['r'].map['d'].isWordEnd)
    self.assertEqual(2, len(self.dictionary.map['a'].map))
    add_keyword(self.dictionary, "ZXCVBNM")
    self.assertTrue(self.dictionary.map['Z'].map['X'].map['C'].map['V'].map['B'].map['N'].map['M'].isWordEnd)

  def test_search_keyword(self):
    print('test_search_keyword\n')
    self.assertEqual(None, search_keyword(self.dictionary, "abc"))
    add_keyword(self.dictionary, "abc")
    self.assertEqual(None, search_keyword(self.dictionary, "bc"))
    self.assertEqual("Exists", search_keyword(self.dictionary, "ab"))
    self.assertEqual("abc", search_keyword(self.dictionary, "abc"))
    self.assertEqual(None, search_keyword(self.dictionary, "abcd"))
    add_keyword(self.dictionary, "ab")
    self.assertEqual("abc", search_keyword(self.dictionary, "abc"))
    self.assertEqual("ab", search_keyword(self.dictionary, "ab"))
    add_keyword(self.dictionary, "a")
    self.assertEqual("abc", search_keyword(self.dictionary, "abc"))
    self.assertEqual("ab", search_keyword(self.dictionary, "ab"))
    self.assertEqual("a", search_keyword(self.dictionary, "a"))
    pass
  
  def test_remove_keyword(self):
    print('test_remove_keyword\n')
    self.assertFalse(remove_keyword(self.dictionary, "bob"))
    add_keyword(self.dictionary, "abc")
    add_keyword(self.dictionary, "ab")
    add_keyword(self.dictionary, "xyz")
    add_keyword(self.dictionary, "xqe")
    add_keyword(self.dictionary, "e")
    self.assertEqual(3, len(self.dictionary.map))
    remove_keyword(self.dictionary, "abc")
    self.assertTrue(self.dictionary.map['a'].map['b'].isWordEnd)
    remove_keyword(self.dictionary, "ab")
    self.assertFalse("a" in self.dictionary.map)
    remove_keyword(self.dictionary, "xyz")
    self.assertTrue("x" in self.dictionary.map)
    self.assertFalse("y" in self.dictionary.map["x"].map)
    remove_keyword(self.dictionary, "xqe")
    self.assertFalse("x" in self.dictionary.map)
    remove_keyword(self.dictionary, "e")
    self.assertFalse("e" in self.dictionary.map)
    self.assertEqual(0, len(self.dictionary.map))


  def test_read_and_solve(self):
    print('test_read_and_solve\n')
    print('\tFile: test1\n')
    self.assertEqual("TEST Not Found", read_and_solve('tests/testFiles/test1.txt').strip())
    print('\tFile: test2\n')
    self.assertEqual("DB 1:0 0:1\nCEG 0:2 2:0\nAEI 0:0 2:2\nIEA 2:2 0:0", read_and_solve('tests/testFiles/test2.txt').strip())
    print('\tFile: test3\n')
    self.assertEqual("YXI 4:0 2:2\nYXG 4:2 2:0", read_and_solve('tests/testFiles/test3.txt').strip())
    print('\tFile: test4\n')
    self.assertEqual("HELLO 0:0 4:4\nGOOD 4:0 4:3\nBYE 1:3 1:1", read_and_solve('tests/testFiles/test4.txt').strip())
    print('\tFile: test5\n')
    self.assertEqual("X 0:0 0:0", read_and_solve('tests/testFiles/test5.txt').strip())
    print('\tFile: test6\n')
    self.assertEqual("ABC 0:0 0:2\nCBA 0:2 0:0\nGHI 2:0 2:2", read_and_solve('tests/testFiles/test6.txt').strip())
    print('\tFile: test7\n')
    self.assertEqual("GEC 2:0 0:2\nHFX 2:1 0:3\nYXI 0:4 2:2", read_and_solve('tests/testFiles/test7.txt').strip())



if __name__ == '__main__':
    unittest.main()